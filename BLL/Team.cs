using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL_Petanque
{
    public class Team
    {
        private string strTeamnaam;
        private int intScore;
        private int intWinningGames;

        public Team()
        {
            strTeamnaam = "onbekend";
            intWinningGames = 0;
            intScore = 0;
        }

        public string Teamnaam
        {
            get { return strTeamnaam; }
            set { strTeamnaam = value; }
        }

        public int Score
        {
            get { return intScore; }
            set { intScore = value; }
        }

        public int WinningGames
        {
            get { return intWinningGames; }
            set { intWinningGames = value; }
        }

        public string Scoren(int intBehaaldeScore) 
        {
            if (intBehaaldeScore > 6)
            {
                return "Score te hoog";
            }
            else if (intBehaaldeScore < 1)
            {
                return "Score te klein";
            }
            else
            {
                intScore = intScore + intBehaaldeScore;
                return "Score bijgewerkt";
            }
            /*Een methode om de score bij te werken: deze krijgt het aantal ballen die scoren 
            binnen.Indien deze score groter is dan 6 of kleiner dan 1, moet de melding 
            teruggegeven worden dat deze score niet correct is. In het andere geval wordt
            de score verhoogt met het aantal ballen die scoren en wordt de melding
            teruggegeven “Score bijgewerkt”.*/
        }

        public void Reset() 
        {
            intScore = 0;
        }


    }
}

